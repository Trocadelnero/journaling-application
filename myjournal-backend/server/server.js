if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}
const express = require('express');
const cors = require('cors');
const app = express();
const { PORT = 3000 } = process.env;
const { Sequelize, QueryTypes } = require('sequelize');


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


const sequelize = new Sequelize( {
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 1000
    }
});





// async function connect() {
//     try {
//         await sequelize.authenticate();
//         console.log('Connection established successfully');
//         // const countries = await sequelize.query('SELECT * FROM country', 
//         // {type: QueryTypes.SELECT
//         // });
//         //[0] added to not spam console
//         // console.log(countries[0];
//         await sequelize.close();   
//     }
//     catch(e) {
//         console.error(e);
        
//     }
// };

// connect();

//1. Get all journals
app.get('/v1/api/journals', async (req, res) => {
try {
await sequelize.authenticate();
console.log('Connection established...');

const journals = await sequelize.query('SELECT * FROM journal', {
    type: QueryTypes.SELECT
});

return res.status(200).json(journals);
}
catch (e) {
console.error(e);
};
});

app.listen(PORT, () => console.log(`Server start on port ${PORT}...`));

module.exports = app;