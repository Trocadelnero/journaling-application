# Journal-application

Mini-project: Application to keep track of journal entries.

## Description
A back-end server made using NodeJS, Express and Sequelize(mySQL).
Front-end is simple HTML, CSS & JavaScript.

## Getting Started

### Dependencies

* "cors": "^2.8.5",
* "dotenv": "^8.2.0",
* "express": "^4.17.1",
* "mysql2": "^2.1.0",
* "sequelize": "^5.21.5"

### Installing

* How/where to download your program
* Any modifications needed to be made to files/folders

### Executing program

* How to run the program
* Step-by-step bullets
```

```
### Authors
**Jonah DelNero**

## Acknowledgments

Inspiration, code snippets, etc.
* [awesome-readme](https://github.com/matiassingers/awesome-readme)
* [dbader](https://github.com/dbader/readme-template)
 
